import './App.css';
import Loader from "react-loader-spinner";
import logo from './CondorCoders_LongNoBG_FullColour.png'

function App() {
  return (
    <div className="loadersContainer">
      <h3>React Loader Spinner Ejemplo</h3>
      <div className="loaders">
        <Loader type="Puff" color="#fff" height={100} width={100}/>
        <Loader type="Audio" color="#fff" height={100} width={100}/>
        <Loader type="Bars" color="#fff" height={100} width={100}/>
        <Loader type="Circles" color="#fff" height={100} width={100}/>
      </div>
        <img src={logo}/>
        <i>https://gitlab.com/condorcoders</i>
    </div>
  );
}

export default App;
